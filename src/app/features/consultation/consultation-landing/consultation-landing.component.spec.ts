import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultationLandingComponent } from './consultation-landing.component';

describe('ConsultationLandingComponent', () => {
	let component: ConsultationLandingComponent;
	let fixture: ComponentFixture<ConsultationLandingComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ConsultationLandingComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ConsultationLandingComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
